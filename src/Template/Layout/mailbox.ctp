<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inbox</title>
    <?= $this->Html->meta('icon') ?>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <?= $this->Html->css('style.css') ?>
    <!-- Latest compiled and minified JavaScript -->
   
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.js"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.12.0/jquery.validate.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <?= $this->fetch('script') ?>
    <script>
        $(document).ready(function(){
            if (window.location.hash == '') {
                window.location.hash = '#INBOX';
            }
        });
    </script>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-sm-3 col-md-2">
           <a href="/">
                 <img width="110px" src="http://media.apnarm.net.au/img/site/logo/bundaberg_news-mail-sgen4pouqj5k4iew9k2.png">
           </a>
        </div>
        <div class="col-sm-9 col-md-10">
            <button type="button" class="btn btn-default" data-toggle="tooltip" title="Refresh" onclick="refresh()">
                &nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-refresh"></span>&nbsp;&nbsp;&nbsp;</button>
            <!-- Single button -->
            <button type="button" class="btn btn-default" data-toggle="tooltip" title="Delete" onclick="deleteMsg()">
                &nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-trash"></span>&nbsp;&nbsp;&nbsp;</button>
            <div class="refresh-icon">
                <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-sm-3 col-md-2">
            <div class="compose btn btn-danger btn-sm btn-block" role="button"><i class="glyphicon glyphicon-edit"></i> Compose</div>
            <hr>
            <ul class="nav nav-pills nav-stacked">
                <!-- <li class="active"><a href="#"><span class="badge pull-right">32</span> Inbox </a>
                </li>
                <li><a href="#"><span class="badge pull-right">3</span>Drafts</a></li> -->
                <?php echo $this->element('folderslist'); ?>
            </ul>
        </div>
        <div class="col-sm-9 col-md-10">
            <!-- Tab panes -->
            <div class="tab-content">                
                <div class="tab-pane fade in active" id="home">
                    <div class="list-group">
                        <?= $this->fetch('content') ?>
                    </div>
                </div>
            </div>           
            <div class="row-md-12">              
                <div class="well"> 
                  <a href="https://bitbucket.org/Dumax/">Create by Dumax</a>
                </div>
            </div>
        </div>
    </div>
</div>
<form method="POST" action="/cake/mailbox/sendemail">
    <i id="close-btn" class="fa fa-times"></i>
    <h2></h2>
    <div class="input-wrapper">
       <span>From:</span><input type="email" name="from" required placeholder="Enter a valid email address">
    </div>
    <div class="input-wrapper">
       <span>To:</span><input type="email" name="to" required placeholder="Enter a valid email address"> 
    </div>
    <div class="input-wrapper">
       <textarea name="subject" placeholder="Enter your message..."></textarea>
    </div>
    
    <input type="submit">
</form>

<script>


$(document).ready(function(){

    $(".compose").click(function(){
        $("form").fadeIn();
    });
    $("#close-btn").click(function(){
        $("form").fadeOut();
    });

    $("form").validate({
    
              submitHandler: function(form) {
                $.post($(form).attr('action'), $(form).serialize(), function(data)  {
                        $(form).fadeOut();
                     }
                );
                return false;
              }
        });
});

</script>

<script>
function send_email(from,to,subject){
    $.post("/cake/mailbox/sendemail",{},function(data){
        $('.list-group').html(data);
      })
     .done(function() {
        $('form').fadeIn();
      });
}
</script>


<script>
function toggleStar(Num) {
    var pattern = "div#" + Num + " .star";
    $(pattern).toggleClass("glyphicon-star-empty glyphicon-star");
    var starEmpty = $(pattern).hasClass("glyphicon-star-empty");
    if (starEmpty) {
        $.post("/cake/mailbox/toggleflag",{ id: Num, action: "clear",currentBox:window.location.hash });
    } else {
        $.post("/cake/mailbox/toggleflag",{ id: Num, action: "add",currentBox:window.location.hash });
    };
}
</script>
<script>
function refresh(){
    $('.refresh-icon').css({display: "inline-block"});
    $.post("/cake/mailbox/refresh",{currentBox:window.location.hash},function(data){
        $('.list-group').html(data);
      })
     .done(function() {
        $('.refresh-icon').css({display: "none"});
      });
}
</script>
<script>
function deleteMsg() {
    var checked = [];
    $(':checkbox:checked').each(function () {
        checked.push($(this).val());
    });

    if (checked == false) {
        alert("Nothing to delete");
        $('.refresh-icon').css({display: "none"});
        return;
    };
    $('.refresh-icon').css({display: "inline-block"});
    $.post("/cake/mailbox/deletemsg",{mails:checked,currentBox:window.location.hash},function(data){
        $('.list-group').html(data);
      })
     .done(function() {
        $('.refresh-icon').css({display: "none"});
      });
}
</script>

<script>
function view_email(msgNum){
    $('.refresh-icon').css({display: "inline-block"});
    $.post("/cake/mailbox/viewemail",{currentBox:window.location.hash,msgNum: msgNum},function(data){
        $('.list-group').html(data);
      })
     .done(function() {
        $('.refresh-icon').css({display: "none"});
      });
}

</script>

<script>
function open_current_mailbox(){
    $('.refresh-icon').css({display: "inline-block"});
    $.post("/cake/mailbox/opencurrentmailbox",{currentBox:window.location.hash},function(data){
        $('.list-group').html(data);
      })
     .done(function() {
        $('.refresh-icon').css({display: "none"});
      });
}
</script>
</body>
</html>