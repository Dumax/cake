<?php
namespace App\Model\Table;

use App\Model\Entity\Email;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Emails Model
 *
 */
class EmailsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('emails');
        $this->displayField('id');
        $this->primaryKey('id');

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('from_name', 'create')
            ->notEmpty('from_name');

        $validator
            ->requirePresence('subject', 'create')
            ->notEmpty('subject');

        $validator
            ->requirePresence('udate', 'create')
            ->notEmpty('udate');

        $validator
            ->requirePresence('favorite', 'create')
            ->notEmpty('favorite');

        $validator
            ->requirePresence('unread', 'create')
            ->notEmpty('unread');

        $validator
            ->add('msg_uid', 'valid', ['rule' => 'numeric'])
            ->requirePresence('msg_uid', 'create')
            ->notEmpty('msg_uid');

        $validator
            ->requirePresence('current_box', 'create')
            ->notEmpty('current_box');

        return $validator;
    }
}
