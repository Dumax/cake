<?php
namespace App\Model\Table;

use App\Model\Entity\Folder;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Folders Model
 *
 */
class FoldersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('folders');
        $this->displayField('id');
        $this->primaryKey('id');

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('folder', 'create')
            ->notEmpty('folder');

        $validator
            ->requirePresence('folder_name', 'create')
            ->notEmpty('folder_name');

        $validator
            ->requirePresence('folder_url', 'create')
            ->notEmpty('folder_url');

        $validator
            ->add('unseen', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('unseen');

        return $validator;
    }
}
