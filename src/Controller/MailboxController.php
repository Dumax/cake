<?php

namespace App\Controller;

use Cake\ORM\TableRegistry;

class MailboxController extends AppController
{

    public function index()
    {
        $this->Imap->init();

        $this->viewBuilder()->layout('mailbox');

        $folders = $this->Imap->getListOfBoxes();
        $this->set('folders', $folders);

        $emails = $this->Imap->getListOfEmails();
        $this->set('emails', $emails);

        $unseen = $this->Imap->countUnseenMails();
        $this->set('unseen', $unseen);
    }

    public function toggleFlag()
    {
        $this->Imap->toggle_flag();
    }

    public function deleteMsg()
    {
        $this->Imap->delete_msg();
        $this->Imap->delete_msg_in_db();
        $emails = $this->Imap->getListOfEmails();
        $this->set('emails', $emails);
    }

    public function refresh()
    {
        echo "work";
        // $this->Imap->clearListOfBoxes();
        // $this->Imap->saveListOfBoxesToDB();
        // $folders = $this->Imap->getListOfBoxes();
        // $this->set('folders', $folders);
    }

    public function viewEmail()
    {
        $this->Imap->view_email();
    }

    public function sendEmail()
    {
        $this->Imap->send_email();
    }

    public function openCurrentMailbox()
    {
        //Check empty field current_box in Emails Table
        // if empty when save email
        $currentBox = $this->Imap->getCurrentBox();
        $query = TableRegistry::get('Emails')
            ->find()
            ->where(['current_box' => $currentBox])
            ->count();

        if ($query == 0) {
            $this->Imap->saveListOfEmailsToDB();
        }

        $emails = $this->Imap->getListOfEmails();
        $this->set('emails', $emails);
    }

}
