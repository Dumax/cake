<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Spyc;

class ImapComponent extends Component
{
    public $connection;
    public $config;
    public $server;
    public $currentBox;

    public function init()
    {
        $this->getCurrentConnection();

        if (!TableRegistry::get('Folders')->find()) {
            $this->saveListOfBoxesToDB();
        }

        if (!TableRegistry::get('Emails')->find()->where(['current_box' => 'INBOX'])->count()) {
            $this->saveListOfEmailsToDB();
        }

    }

    public function getCurrentConnection()
    {
        // Connect using YAML config
        $this->config = Spyc::YAMLLoad("../config/config.yaml");
        $this->server = str_replace('INBOX', '', $this->config['server']);
        $this->getCurrentBox();
        $this->connection = imap_open($this->server . $this->currentBox, $this->config['email'], $this->config['password'])
        or die("can't connect: " . imap_last_error());
        return $this->connection;
    }

    public function getCurrentBox($currentBox = "INBOX")
    {
        if ($this->request->is('ajax')) {
            $currentBox = $this->request->data['currentBox'];
            $currentBox = str_replace('_', ' ', $currentBox);
            $currentBox = str_replace("*", "/", $currentBox);
            $currentBox = str_replace("#", "", $currentBox);

            $this->currentBox = $currentBox;
        }

        $this->currentBox = $currentBox;
        return $this->currentBox;
    }

    public function saveListOfBoxesToDB()
    {
        $this->getCurrentConnection();
        $mailboxes = imap_list($this->connection, "{imap.gmail.com:993/imap/ssl}", "*");

        $folders = [];

        foreach ($mailboxes as $mailbox) {
            $unseen = imap_status($this->connection, $mailbox, SA_ALL)->unseen;
            $mailbox = str_replace("{imap.gmail.com:993/imap/ssl}", "", $mailbox);
            $mailbox_url = str_replace("/", "*", $mailbox);
            $mailbox_url = str_replace(" ", "_", $mailbox_url);
            $mailbox_name = str_replace("[Gmail]/", "", $mailbox);
            $folder = [];
            $folder["unseen"] = ($unseen > 0) ? $unseen : '';
            $folder["mailbox"] = $mailbox;
            $folder["mailbox_url"] = $mailbox_url;
            $folder["mailbox_name"] = mb_convert_encoding($mailbox_name, "UTF-8", "UTF7-IMAP");
            $folders[] = $folder;
            $folder = [];
        }

        $queryExistFolders = TableRegistry::get('Folders')->find();

        foreach ($queryExistFolders as $queryExistFolder) {
            $existFolders[] = $queryExistFolder->folder_name;
        }

        foreach ($folders as $box) {

            if (in_array($box["mailbox_name"], $existFolders)) {
                continue;
            }

            $tableFolders = TableRegistry::get('Folders')->query();
            $tableFolders->insert(['folder', 'folder_name', 'folder_url', 'unseen'])
                ->values([
                    'folder' => $box['mailbox'],
                    'folder_name' => $box["mailbox_name"],
                    'folder_url' => $box["mailbox_url"],
                    'unseen' => $box["unseen"],
                ])
                ->execute();
        }
    }

    public function getListOfBoxes()
    {
        $folders = [];
        $query = TableRegistry::get('Folders')->find();

        foreach ($query as $box) {
            $folder = [];
            $folder["unseen"] = ($box->unseen > 0) ? $box->unseen : '';
            $folder["mailbox"] = $box->folder;
            $folder["mailbox_url"] = $box->folder_url;
            $folder["mailbox_name"] = $box->folder_name;
            $folders[] = $folder;
            $folder = [];
        }
        return $folders;
    }

    public function saveListOfEmailsToDB()
    {
        $this->getCurrentConnection();
        $numMessages = imap_num_msg($this->connection);
        $emails = [];

        for ($i = $numMessages; $i > 0; $i--) {
            $header = imap_headerinfo($this->connection, $i);
            $fromInfo = $header->from[0];
            $replyInfo = $header->reply_to[0];

            $details = array(
                "fromName" => (isset($fromInfo->personal))
                ? $fromInfo->personal : "",
                "subject" => (isset($header->subject))
                ? $header->subject : "",
                "udate" => (isset($header->udate))
                ? $header->udate : "",
                "msgId" => (isset($header->Msgno))
                ? $header->Msgno : "",
                "favorite" => (isset($header->Flagged))
                ? $header->Flagged : "",
                "unread" => (isset($header->Unseen))
                ? $header->Unseen : "",
            );

            $details["fromName"] = imap_utf8($details["fromName"]);
            $details["subject"] = imap_utf8($details["subject"]);
            $details["udate"] = date("d/m/Y", $details["udate"]);
            $details["msgId"] = trim($details["msgId"]);
            // $details["favorite"] = ($details["favorite"] == 'F') ? '' : 'glyphicon-star-empty';
            $details["unread"] = ($details["unread"] == 'U') ? 'read' : '';
            $details["msgUid"] = imap_uid($this->connection, $details["msgId"]);
            $emails[] = $details;
        }

        foreach ($emails as $email) {
            $tableEmails = TableRegistry::get('Emails')->query();
            $tableEmails->insert(['from_name', 'subject', 'udate', 'favorite', 'unread', 'msg_uid', 'current_box'])
                ->values([
                    'from_name' => $email["fromName"],
                    'subject' => $email["subject"],
                    'udate' => $email["udate"],
                    'favorite' => $email["favorite"],
                    'unread' => $email["unread"],
                    'msg_uid' => $email["msgUid"],
                    'current_box' => $this->getCurrentBox(),
                ])
                ->execute();
        }

    }

    public function getListOfEmails()
    {
        $this->getCurrentConnection();

        $emails = [];
        $query = TableRegistry::get('Emails')->find()->where(['current_box' => $this->getCurrentBox()]);

        foreach ($query as $msg) {
            $email = [];

            $email["fromName"] = $msg->from_name;
            $email["subject"] = $msg->subject;
            $email["udate"] = $msg->udate;
            $email["favorite"] = ($msg->favorite == "F") ? 'glyphicon-star' : 'glyphicon-star-empty';
            // $email["favorite"] = $msg->favorite;
            $email["unread"] = $msg->unread;
            $email["msgId"] = $msg->msg_uid;

            $emails[] = $email;
            $email = [];
        }
        return $emails;
    }

    public function countUnseenMails()
    {
        $this->getCurrentConnection();

        $totalUnseen = 0;
        $emails = imap_search($this->connection, 'ALL');

        foreach ($emails as $email => $msgNum) {
            if (imap_headerinfo($this->connection, $msgNum)->Unseen == 'U') {
                $totalUnseen++;
            }
        }
        return $totalUnseen;
    }

    public function view_email()
    {
        if ($this->request->is('ajax')) {
            $this->getCurrentConnection();
            $msgNum = $this->request->data['msgNum'];
            $ecod = imap_fetchstructure($this->connection, $msgNum, FT_UID)->parts[1]->encoding;
            $message = imap_fetchbody($this->connection, $msgNum, 2, FT_UID);
            $str = imap_base64($message);
            echo $str;
        }
    }

    public function toggle_flag()
    {
        if ($this->request->is('ajax')) {
            $this->getCurrentConnection();
            $id = $this->request->data["id"];
            $action = $this->request->data["action"];
            $header = imap_headerinfo($this->connection, $id);
            $connection = ConnectionManager::get('default');
            if ($action == 'add') {
                $connection->update('emails', ['favorite' => 'F'], ['msg_uid' => $id]);
                imap_setflag_full($this->connection, $id, "\\Flagged", ST_UID);
            } elseif ($action == 'clear') {
                $connection->update('emails', ['favorite' => ''], ['msg_uid' => $id]);
                imap_clearflag_full($this->connection, $id, "\\Flagged", ST_UID);
            }
        }
    }

    public function delete_msg()
    {
        if ($this->request->is('ajax')) {
            $this->getCurrentConnection();
            foreach ($this->request->data['mails'] as $id) {
                imap_delete($this->connection, $id, FT_UID);
            }
            imap_expunge($this->connection);
        }
    }

    public function delete_msg_in_db()
    {
        if ($this->request->is('ajax')) {
            $this->getCurrentConnection();
            foreach ($this->request->data['mails'] as $id) {
                // $query = TableRegistry::get('Emails')->find()->where(['msg_uid' => $id]);
                $connection = ConnectionManager::get('default');
                $connection->delete('emails', ['msg_uid' => $id]);
            }
        }
    }

    public function clearCurrentBox()
    {
        if ($this->request->is('ajax')) {
            $this->getCurrentConnection();
            $currentBox = $this->getCurrentBox();
            $connection = ConnectionManager::get('default');
            $connection->delete('emails', ['current_box' => $currentBox]);
        }
    }

    public function clearListOfBoxes()
    {
        if ($this->request->is('ajax')) {
            $query = $this->Folders->find('all');
            $this->Folders->delete($query);

        }
    }

}
